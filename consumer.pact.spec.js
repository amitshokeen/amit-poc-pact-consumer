// (1) Import the pact library and matching methods
const { Pact } = require ('@pact-foundation/pact');
const { User_languages_ApiClient } = require ('./api');
const { User_languages } = require ('./user_languages');
const { like, regex } = require ('@pact-foundation/pact/dsl/matchers');
const chai = require("chai")
const expect = chai.expect

// (2) Configure our Pact library
const mockProvider = new Pact({
  consumer: 'get_user_languages',
  provider: 'backend',
  cors: true // needed for katacoda environment
});

describe('get-user-languages API test', () => {
  // (3) Setup Pact lifecycle hooks
  before(() => mockProvider.setup());
  afterEach(() => mockProvider.verify());
  after(() => mockProvider.finalize());

  it('get user languages', async () => {
    // (4) Arrange
    const expectedJSON = [
        {
            "languageID": 1,
            "LanguageName": "English",
            "Locale": "en-AU"
        },
        {
            "LanguageID": 8,
            "LanguageName": "한국어",
            "Locale": "ko"
        },
        {
            "LanguageID": 45,
            "LanguageName": "简体中文",
            "Locale": "zh"
        },
        {
            "LanguageID": 46,
            "LanguageName": "繁體中文",
            "Locale": "zh-TW"
        }
    ]

    await mockProvider.addInteraction({
      state: 'a set of user languages exists',
      uponReceiving: 'a request to get user languages',
      withRequest: {
        method: 'GET',
        path: '/v1.0/settings/get-user-languages'
      },
      willRespondWith: {
        status: 200,
        headers: {
          'Content-Type': regex({generate: 'application/json; charset=utf-8', matcher: '^application\/json'}),
        },
        body: like(expectedJSON),
      },
    });

    // (5) Act
    const api = new User_languages_ApiClient(mockProvider.mockService.baseUrl);
    const lang = await api.getLang();

    // (6) Assert that we got the expected response
    expect(lang).to.deep.equal(new User_languages(1, 'English', 'en-AU'));
  });
});
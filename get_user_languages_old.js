const fetch = require("node-fetch");
const post_url = "https://api.mediaportal.com/V1.0/iam/login";
const get_url = "https://api.isentia.io/v1.0/settings/get-user-languages";
const api_name = "/v1.0/settings/get-user-languages | ";

let get_user_languages = async function () {
  try {
    //let start = new Date();
    let post_response = await fetch(post_url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: "analytics02",
        password: "analytics02",
        client_type: "web",
      }),
    });
    let post_parsedResponse = await post_response.json();
    if (post_parsedResponse.status === "success") {
      let get_response = await fetch(get_url, {
        method: "GET",
        headers: { Authorization: "Bearer " + post_parsedResponse.token },
      });
        //let get_responseParsed = await get_response.json();
        //console.log(get_responseParsed);
        //return get_responseParsed;
        return get_response.json();
    } else {
      console.log(api_name + "Something is wrong with the auth-token stuff.");
    }
  } catch (err) {
    console.log("Error from try-catch block: " + err);
  }
};

// module.exports = {
//   get_user_languages
// }

get_user_languages().then(x => {
  console.log(x);
});
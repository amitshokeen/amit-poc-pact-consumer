const axios = require('axios');
const { User_languages } = require('./user_languages');

class User_languages_ApiClient {
  constructor(url) {
    this.url = url
  }

  async getLang() {
    return axios.get(`${this.url}/v1.0/settings/get-user-languages`)
    .then(r => new User_languages(r.data[0].languageID, r.data[0].LanguageName, r.data[0].Locale));
  }
}
module.exports = {
    User_languages_ApiClient
}